class GlobalProps {
  constructor() {
    this.props = hive.api.getDynamicGlobalPropertiesAsync();
    this.median = async () => {console.log("replace get_median")};
    this.fund = hive.api.getRewardFundAsync("post");
    this.prices = this.initGetPrice();
  }
  async getProp(key) {
    return (await this.props)[key];
  }
  async getMedian() {
    return await this.median;
  }
  async getFund(key) {
    return (await this.fund)[key];
  }
  async initGetPrice() {
    let price = await getPricesAsync();
    return price
  }
  async getPrices() {
    console.log(this.prices)
    return this.prices
  }
}
